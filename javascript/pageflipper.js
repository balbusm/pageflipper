/**
 * Copyright (C) 2013 Mateusz Balbus <balbusm@gmail.com> - https://bitbucket.org/balbusm/pageflipper 
 * 
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * 
 *         http://www.apache.org/licenses/LICENSE-2.0
 * 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

/**
 * PageFlipper based on Rick Barraza tutorial http://rbarraza.com/html5-canvas-pageflip
**/
 
function PageFlipper(aPageWidth, aPageHeight, aPages, aConfig) {
    
    var config = aConfig === undefined ? {} : aConfig;
    
    var DEBUG = config.debug === undefined ? false : config.debug;

    var SHADOW_BACK_IMG = config.shadowBlackImg === undefined ? "shadowBack.png" : config.shadowBlackImg;
    var SHADOW_CURVE_IMG = config.shadowCurveImg === undefined ? "shadowCurve.png" : config.shadowCurveImg;
    var LOADING_IMG = config.loadingImg === undefined ? "loading.gif" : config.loadingImg;
    var ELEM_PAGEFLIP_CLASS = config.divPageFlipClass === undefined ? "DivPageFlip" : config.divPageFlipClass;
    
    var WIDTH;
    var HEIGHT;
    var DIAGONAL;

    var paddingVertical = 40;
    var paddingHorizontal = 40;

    var MAX_DIRTY_COUNT = 80;

    var ELEM_LOADING_ID = "imgLoading";

    var visualDebugger;

    var pointerFusion;

    var pageLeft;
    var pageTop;
    var pageWidth = 2 * aPageWidth;
    var pageHalfWidth;
    var pageHeight = aPageHeight;
    var pageHalfHeight;
    var innerRadius;
    var outerRadius;
    var distanceFromTop;
    var bisectorAngle;
    var bisectorTangent;
    var foldBorder;

    var dx;
    var dy;
    var angle2follow;

    var spineTop;
    var spineCenter;
    var spineBottom;
    var edgeBottom;
    var corner;

    var mouse = new Point(0, 0);
    var follow = new Point(0, 0);
    var radius1 = new Point(0, 0);
    var radius2 = new Point(0, 0);
    var bisector = new Point(0, 0);
    var bisectorBottom = new Point(0, 0);
    var tangentBottom = new Point(0, 0);

    var clipPoint0;
    var clipPoint1;
    var clipPoint2;
    var clipPoint3;

    var srcPages = aPages;
    var pages = new Array(srcPages.length);
    var currentPageLeft = 0;
    var currentPageRight = currentPageLeft + 1;

    var nextPageLeft = currentPageRight + 1;
    var nextPageRight = nextPageLeft + 1;

    var previousPageLeft = currentPageLeft - 2;
    var previousPageRight = previousPageLeft + 1;

    var imgLoading;
    var imgShadowBack;
    var imgShadowCurve;
    var assetsLoaded = 0;
    var totalAssets;

    var divTarget;
    var canvas;
    var ctx;
    var sheetCanvas;
    var sheetctx;
    var clipCanvas;
    var clipctx;
    var shadowsCanvas;
    var shadowsctx;

    var pageTurnStrategy;

    var dirtyCount = MAX_DIRTY_COUNT;

    function Point(x, y) {
        this.x = x;
        this.y = y;
    }

    function createVisiualDebugger(enable) {
        var visualDebugger = {};
        if (enable) {
            
            visualDebugger.drawCircle = function (context, color, label, x, y, r) {
                context.fillStyle = color;
                context.beginPath();
                context.arc(x, y, r, 0, Math.PI * 2, true);
                context.closePath();
                context.fill();
                context.fillStyle = "#FFFFFF";
                context.fillText(label, x, y + 5);
            }

            visualDebugger.drawBoundaryRing = function (context, color, x, y, r) {
                context.strokeStyle = color;
                context.fillStyle = "rgba(0,0,0,0.1)";
                context.beginPath();
                context.arc(x, y, r, 0, Math.PI * 2, true);
                context.closePath();
                context.fill();
                context.stroke();
            }

            visualDebugger.drawRectangle = function (context, color, x, y, w, h) {
                context.fillStyle = color;
                context.beginPath();
                context.rect(x, y, w, h);
                context.closePath();
                context.fill();
            }

            visualDebugger.drawLines = function (context, color, coordinates) {
                context.strokeStyle = color;
                context.beginPath();
                var firstValue = true;
                for (var i = 0; i < coordinates.length; i++) {
                    if (firstValue)
                        context.moveTo(coordinates[i].x, coordinates[i].y);
                    else
                        context.lineTo(coordinates[i].x, coordinates[i].y);
                    firstValue = false;
                }
                
                context.closePath();
                context.stroke();
            }


        } else {
            visualDebugger.drawCircle = function () {};
            visualDebugger.drawBoundaryRing = function () {};
            visualDebugger.drawRectangle = function () {};
            visualDebugger.drawLines = function () {};
        }

        return visualDebugger;
    }

    function init() {
        visualDebugger = createVisiualDebugger(DEBUG);

        // SET UP THE EVENT HANDLERS FOR POINTER FUSION
        var selectedDivTarget = false;
        if (document.querySelector) {
            divTarget = document.querySelector("." + ELEM_PAGEFLIP_CLASS);
            if (divTarget) {
                selectedDivTarget = true;
                pointerFusion = new PointerFusion(divTarget, onMouseDown, onMouseMove, onMouseUp);
            }
        }

        if (!selectedDivTarget) {
            // querySelector or "DivPageFlip" not available
            // terminate initialization
            return;
        }
		
        

        // init with next page turn strategy
        pageTurnStrategy = new NextPageTurnStrategy();

        canvas = createCanvas();
        // canvas not supported
        if (!canvas.getContext2) {
            return;
        }

        ctx = canvas.getContext2("2d");
        ctx.font = "12px Arial";
        ctx.textAlign = "center";
        canvas.style.position = "absolute";
        divTarget.appendChild(canvas);

        sheetCanvas = createCanvas();
        sheetctx = sheetCanvas.getContext2("2d");

        clipCanvas = createCanvas();
        clipctx = clipCanvas.getContext2("2d");

        shadowsCanvas = createCanvas();
        shadowsctx = shadowsCanvas.getContext2("2d");

        window.addEventListener('resize', setSizes, false);
        window.addEventListener('orientationchange', setSizes, false);

        setSizes();
        loadAssets();
    }
    
    function createCanvas() {
        newCanvas = document.createElement("canvas");
        if (newCanvas.getContext) {
            newCanvas.getContext2 = function(type) {
                newContext = this.getContext(type);
                if (type == "2d") {
                    newContext.drawImage2 = function(img) {
                        if (img != null) {
                            var i = 0;
                            if (arguments.length == 3) {
                                var x = arguments[++i];
                                var y = arguments[++i];
                                this.drawImage(img, x, y);
                            } else if (arguments.length == 5) {
                                var x = arguments[++i];
                                var y = arguments[++i];
                                var width = arguments[++i];
                                var height = arguments[++i];
                                this.drawImage(img, x, y, width, height);
                            } else if (arguments.length == 9) { 
                                var sx = arguments[++i];
                                var sy = arguments[++i];
                                var swidth = arguments[++i];
                                var sheight = arguments[++i];
                                var x = arguments[++i];
                                var y = arguments[++i];
                                var width = arguments[++i];
                                var height = arguments[++i];
                                this.drawImage(img, sx, sy, swidth, sheight, x, y, width, height);
                            }                            

                        }
                    }
                }
                return newContext;
            }

        }
        return newCanvas;
    }

    function setSizes() {

        divTarget.style.height = (pageHeight + paddingVertical) + "px";
        divTarget.style.width = (pageWidth + paddingHorizontal) + "px";

        WIDTH = pageWidth + paddingHorizontal;
        HEIGHT = pageHeight + paddingVertical;
        DIAGONAL = Math.ceil(Math.sqrt(WIDTH * WIDTH + HEIGHT * HEIGHT));
        pageLeft = paddingHorizontal * .5;
        // pageHalfWidth = WIDTH * .48;
        // pageHeight = pageHalfWidth*1.4;
        pageTop = paddingVertical * .5;
        pageHalfHeight = pageHeight * .5;
        pageHalfWidth = pageWidth * .5;
        
        var foldBorderX = pageHalfWidth * .2;
        var foldBorderY = pageHalfHeight * .2;
        // take smalest fold border
        foldBorder = foldBorderX < foldBorderY ? foldBorderX : foldBorderY;
        innerRadius = pageHalfWidth;
        outerRadius = Math.sqrt(pageHalfWidth * pageHalfWidth + pageHeight * pageHeight);
        canvas.width = WIDTH;
        canvas.height = HEIGHT;

        spineTop = new Point(pageLeft + pageHalfWidth, pageTop);
        spineCenter = new Point(pageLeft + pageHalfWidth, pageTop + pageHalfHeight);
        spineBottom = new Point(pageLeft + pageHalfWidth, pageTop + pageHeight);
        radius1 = new Point(0, 0);
        setPageTurnSizes();

        sheetCanvas.width = pageHalfWidth * 2;
        sheetCanvas.height = pageHeight;

        clipCanvas.width = WIDTH;
        clipCanvas.height = HEIGHT;
        shadowsCanvas.width = pageHalfWidth * 2.001;
        shadowsCanvas.height = pageHeight * 1.001;
        dirtyCount = MAX_DIRTY_COUNT;

    }

    function setPageTurnSizes() {
        edgeBottom = pageTurnStrategy.createEdgeBottomPoint();
        corner = pageTurnStrategy.createCornerPoint();
        mouse = pageTurnStrategy.createMousePoint();
        follow = pageTurnStrategy.createFollowPoint();
    }

    function loadAssets() {

        totalAssets = 0;

        imgLoading = document.createElement("img");
        imgLoading.src = LOADING_IMG;
        imgLoading.onload = displayLoading;

        for ( var i = 0; i < srcPages.length; i++) {
            totalAssets++;
            var imgPage = document.createElement("img");
            imgPage.src = srcPages[i];
            imgPage.onload = checkAllLoaded;
            pages[i] = imgPage;
        }

        // add first page blank
        imgBlank = null;
        pages.unshift(imgBlank);

        // add last page blank
        // only when even number of pages
        if (srcPages.length % 2 == 0)
            pages.push(imgBlank);

        totalAssets++;
        imgShadowBack = document.createElement("img");
        imgShadowBack.src = SHADOW_BACK_IMG;
        imgShadowBack.onload = checkAllLoaded;

        totalAssets++;
        imgShadowCurve = document.createElement("img");
        imgShadowCurve.src = SHADOW_CURVE_IMG;
        imgShadowCurve.onload = checkAllLoaded;

    }

    function checkAllLoaded() {
        assetsLoaded++;
        if (assetsLoaded == totalAssets) {
            setupSheets();
            return setInterval(draw, 10);
        } else {
            return 0;
        }
    }

    function displayLoading() {
        if (assetsLoaded < totalAssets) {
            var divLoading = document.createElement("div");
            divLoading.setAttribute("id", ELEM_LOADING_ID);
            divLoading.style.textAlign = "center";
            divLoading.style.display = "table-cell";
            divLoading.style.verticalAlign = "middle";
            divLoading.style.height = divTarget.style.height;
            divLoading.style.width = divTarget.style.width;
            divLoading.appendChild(imgLoading);
            divTarget.appendChild(divLoading);
        }

    }

    function clear() {
        ctx.fillStyle = "#333333";
        ctx.clearRect(0, 0, WIDTH, HEIGHT);
    }

    function setupSheets() {
        // remove loading image
        var selectedImgLoading = divTarget.querySelector("#" + ELEM_LOADING_ID);
        if (selectedImgLoading)
            divTarget.removeChild(selectedImgLoading);

        pointerFusion.refreshState();

        sheetctx.drawImage2(pageTurnStrategy.getTurnPageLeft(), 0, 0, pageHalfWidth, pageHeight);
        sheetctx.drawImage2(pageTurnStrategy.getTurnPageRight(), pageHalfWidth, 0, pageHalfWidth, pageHeight);
        shadowsctx.drawImage2(imgShadowBack, spineBottom.x, 0);
    }

    function drawSheets() {
        var pageAngle = pageTurnStrategy.calcPageAngle();

        var clipAngle = pageTurnStrategy.calcClipAngle();
        if (clipAngle < 0)
            clipAngle += Math.PI;
        clipAngle -= Math.PI / 2;

        var distanceFromTarget = pageTurnStrategy.calcDistanceFromTarget(corner);
        var shadowsAlpha = distanceFromTarget;

        // CALCULATE THE CLIPPING CORNERS
        clipPoint0 = rotateClipPoint(pageTurnStrategy.calcClipPoint0(), clipAngle);
        clipPoint1 = rotateClipPoint(pageTurnStrategy.calcClipPoint1(), clipAngle);
        clipPoint2 = rotateClipPoint(pageTurnStrategy.calcClipPoint2(), clipAngle);
        clipPoint3 = rotateClipPoint(pageTurnStrategy.calcClipPoint3(), clipAngle);

        // RESET THE CLIPCANVAS AND CREATE CLIPPING REGION
        clipCanvas.width = WIDTH;
        clipctx.beginPath();
        clipctx.moveTo(clipPoint0.x, clipPoint0.y);
        clipctx.lineTo(clipPoint1.x, clipPoint1.y);
        clipctx.lineTo(clipPoint2.x, clipPoint2.y);
        clipctx.lineTo(clipPoint3.x, clipPoint3.y);
        clipctx.closePath();
        clipctx.clip();
        visualDebugger.drawLines(clipctx, "#000", [clipPoint0, clipPoint1, clipPoint2, clipPoint3]);

        // draw current page
        drawFramework(clipctx);

        // UPDATE THE PAGE BEING TURNED AND INCLUDE CURVE SHADOW
        sheetctx.clearRect(0, 0, pageHalfWidth, pageHeight);
        sheetctx.drawImage2(pageTurnStrategy.getTurnPageLeft(), 0, 0, pageHalfWidth, pageHeight);
        sheetctx.drawImage2(pageTurnStrategy.getTurnPageRight(), pageHalfWidth, 0, pageHalfWidth, pageHeight);

        sheetctx.save();
        sheetctx.globalAlpha = shadowsAlpha * .5;
        var calcCurveX = pageHalfWidth - (tangentBottom.x - (pageHalfWidth + pageLeft));
        var imgShadowCurvePos = pageTurnStrategy.getImgShadowCurvePosition();
        // rotate so shadow image is mirrored
        sheetctx.translate(calcCurveX, tangentBottom.y - pageTop);
        sheetctx.rotate(pageTurnStrategy.calcImgShadowCurveAngle(-clipAngle));
        
        visualDebugger.drawRectangle(sheetctx, "#0000FF", imgShadowCurvePos.x, imgShadowCurvePos.y, imgShadowCurve.width, DIAGONAL);

        sheetctx.drawImage2(imgShadowCurve, imgShadowCurvePos.x, imgShadowCurvePos.y, imgShadowCurve.width, DIAGONAL);
        sheetctx.restore();

        // DRAW THE UPDATED PAGE BEING TURNED
        clipctx.translate(corner.x, corner.y);
        clipctx.rotate(pageAngle);
        turnedPagePosition = pageTurnStrategy.getTurnedPagePosition();
        clipctx.drawImage2(sheetctx.canvas, turnedPagePosition.x, turnedPagePosition.y);

        // DRAW UNDER SHADOWS
        shadowsCanvas.width = pageHalfWidth * 2;

        edgeOffsetPosition = pageTurnStrategy.getEdgeOffsetPoint();

        shadowsctx.translate(-pageLeft, -pageTop);
        shadowsctx.beginPath();
        shadowsctx.moveTo(clipPoint0.x, clipPoint0.y);
        shadowsctx.lineTo(clipPoint3.x, clipPoint3.y);
        shadowsctx.lineTo(edgeOffsetPosition.x, clipPoint3.y);
        shadowsctx.lineTo(edgeOffsetPosition.x, clipPoint0.y);
        shadowsctx.closePath();
        shadowsctx.clip();
        visualDebugger.drawLines(shadowsctx, "#000", [
            clipPoint0, 
            clipPoint3,
            new Point(edgeOffsetPosition, clipPoint3),
            new Point(edgeOffsetPosition, clipPoint0)
            ]);


        visualDebugger.drawCircle(ctx, "rgba(255,0,0,1.0)", "C1", clipPoint0.x, clipPoint0.y, 10);
        visualDebugger.drawCircle(ctx, "rgba(255,0,0,1.0)", "C3", clipPoint3.x, clipPoint3.y, 10);
        visualDebugger.drawCircle(ctx, "rgba(255,0,0,1.0)", "W3", 0, clipPoint3.y, 10);
        visualDebugger.drawCircle(ctx, "rgba(255,0,0,1.0)", "W0", 0, clipPoint0.y, 10);

        shadowsctx.translate(pageLeft, pageTop);

        shadowsctx.globalAlpha = 1.0;
        shadowsctx.drawImage2(pageTurnStrategy.getTurnPageLeft(), 0, 0, pageHalfWidth, pageHeight);
        shadowsctx.drawImage2(pageTurnStrategy.getTurnPageRight(), pageHalfWidth, 0, pageHalfWidth, pageHeight);
        shadowsctx.globalAlpha = shadowsAlpha * 1.4;

        // rotate so shadow image is mirrored
        shadowsctx.translate(tangentBottom.x - pageLeft, tangentBottom.y - pageTop);
        shadowsctx.rotate(pageTurnStrategy.calcImgShadowBackAngle(clipAngle));

        var imgShadowBackPos = pageTurnStrategy.getImgShadowBackPosition();
        
        shadowsctx.drawImage2(imgShadowBack, imgShadowBackPos.x, imgShadowBackPos.y, imgShadowBack.width, DIAGONAL);
        visualDebugger.drawRectangle(shadowsctx, "#00FF00", imgShadowBackPos.x, imgShadowBackPos.y, imgShadowBack.width, DIAGONAL);

        ctx.drawImage2(shadowsctx.canvas, pageLeft, pageTop);

        // DRAW THE CORNER
        ctx.drawImage2(clipctx.canvas, 0, 0);
    }

    function rotateClipPoint(_p, angle) {
        var result = new Point();
        _p.x -= tangentBottom.x;
        _p.y -= tangentBottom.y;
        result.x = (_p.x * Math.cos(angle)) - (_p.y * Math.sin(angle));
        result.y = Math.sin(angle) * _p.x + Math.cos(angle) * _p.y;
        result.x += tangentBottom.x;
        result.y += tangentBottom.y;
        return result;
    }

    function startTurn() {
        mouse = pageTurnStrategy.getMouseDestinationPoint();
        isTurning = true;
        turningCount = 60;
        isMouseOver = false;
        isMouseDown = false;
        isDrag = false;
    }

    function turnPage() {
        if (turningCount > 0) {
            dirtyCount = MAX_DIRTY_COUNT;
            turningCount--;
            mouse = pageTurnStrategy.getMouseDestinationPoint();
        } else {
            // swapEverything...
            currentPageLeft = pageTurnStrategy.getTurnPageLeftNumber();
            currentPageRight = pageTurnStrategy.getTurnPageRightNumber();
            nextPageLeft = currentPageRight + 1;
            nextPageRight = nextPageLeft + 1;
            previousPageLeft = currentPageLeft - 2;
            previousPageRight = previousPageLeft + 1;
            mouse = pageTurnStrategy.createMousePoint();
            follow = pageTurnStrategy.createFollowPoint();
            isTurning = false;
        }
    }

    function draw() {
        if (dirtyCount > 0) {
            if (isTurning)
                turnPage();
            var oldFollow = new Point(follow.x, follow.y);
            renderMath();
            dirtyCount--;
            // check if there is any change in the canvas
            if (dirtyCount < MAX_DIRTY_COUNT - 1 && oldFollow.x == follow.x && oldFollow.y == follow.y)
                return;

            clear();
            drawSheets();
        }
    }

    function drawFramework(canvasCtx) {
        canvasCtx.drawImage2(pages[currentPageLeft], pageLeft, pageTop, pageHalfWidth, pageHeight);
        canvasCtx.drawImage2(pages[currentPageRight], pageLeft + pageHalfWidth, pageTop, pageHalfWidth, pageHeight);
    }

    function renderMath() {

        // STEP 1: CONSTRAINT THE CORNER TO THE INNER RADIUS (BOTTOM) OF THE
        // BOOK SPINE

        // EASE THE FOLLOW [F] TOWARD THE MOUSE [M]
        follow.x += (mouse.x - follow.x) * .15;
        follow.y += (mouse.y - follow.y) * .15;

        // CHECK DISTANCE FROM SPINE BOTTOM [SB] TO FOLLOW [F]
        dx = spineBottom.x - follow.x;
        dy = spineBottom.y - follow.y;

        // DETERMINE THE ANGLE FROM SPINE BOTTOM [SB] TO FOLLOW [F]
        // AND PLOT THE INNER RADIUS CONSTRAINT [R1]
        angle2follow = Math.atan2(dy, dx);
        radius1.x = spineBottom.x - Math.cos(angle2follow) * innerRadius;
        radius1.y = spineBottom.y - Math.sin(angle2follow) * innerRadius;

        // IF THE FOLLOW [F] IS OUTSIDE THE INNER RADIUS CONSTRAINT [R1],
        // CONSTRAIN THE PAGE CORNER [C] TO THE INNER RADIUS [R1],
        // OTHERWISE, MATH THE CORNER [C] WITH OUR EASED MOUSE FOLLOW [F]
        distanceFromTop = Math.sqrt((dy * dy) + (dx * dx));
        if (distanceFromTop > innerRadius) {
            corner.x = radius1.x;
            corner.y = radius1.y;
        } else {
            corner.x = follow.x;
            corner.y = follow.y;
        }

        // STEP 2A: CONSTRAINT THE CORNER TO THE OUTER RADIUS (TOP) OF THE BOOK
        // SPINE

        // WHAT DIRECTION FROM THE SPINE TOP IS THE CORNER
        dx = spineTop.x - corner.x;
        dy = spineTop.y - corner.y;
        angle2follow = Math.atan2(dy, dx);

        // USING THE ABOVE, VISUALIZE THE OUTER RADIUS CONSTRAINT
        radius2.x = spineTop.x - Math.cos(angle2follow) * outerRadius;
        radius2.y = spineTop.y - Math.sin(angle2follow) * outerRadius;

        // CONSTRAINT THE CORNER [C] TO THE OUTER RADIUS [R2] IF IT FALLS
        // OUTSIDE IT
        var distanceToCorner = Math.sqrt(dx * dx + dy * dy);
        if (distanceToCorner > outerRadius) {
            corner.x = radius2.x;
            corner.y = radius2.y;
        }

        // STEP 2B: CALCULATE THE CRITICAL TRIANGLE [T0, T1, T2] FORMED BY
        // FLIPPING A PAGE BETWEEN THE
        // CORNER OF THE PAGE AND THE PAGE BOTTOM TO DETERMINE ROTATION OF THE
        // PAGE BEING FLIPPED

        // [T0] IS THE BISECTOR BETWEEN THE CORNER [C] AND THE EDGE BOTTOM [EB].
        // THIS DETERMINES HOW MUCH
        // OF THE "BACK" OF THE PAGE WILL BE SHOWN.
        bisector.x = corner.x + .5 * (edgeBottom.x - corner.x);
        bisector.y = corner.y + .5 * (edgeBottom.y - corner.y);

        // [T1] IS THE TANGENT OF THE BISECTOR SHOOTING OFF AT 90 DEGREES TO THE
        // BOTTOM OF THE PAGE
        bisectorAngle = Math.atan2(edgeBottom.y - bisector.y, edgeBottom.x - bisector.x);
        bisectorTangent = bisector.x - Math.tan(bisectorAngle) * (edgeBottom.y - bisector.y);

        // THIS CHECK WILL CONSTRAINT THE BISECTOR TANGENT [T1] TO THE SPINE
        // BOTTOM [SB] ONCE RENDERED
        if (bisectorTangent < 0)
            bisectorTangent = 0;
        tangentBottom.x = bisectorTangent;
        tangentBottom.y = edgeBottom.y;

        // [T2] IS THE SIMPLE RIGHT CORNER OF THE TWO POINTS
        bisectorBottom.x = bisector.x;
        bisectorBottom.y = tangentBottom.y;

    }

    var testMouse = new Point();
    var isMouseOver = false;
    var isMouseDown = false;
    var isDrag = false;
    var isTurning = false;
    var turningCount = 0;

    function checkPageInBoundry(pageNumber) {
        if (pageNumber < pages.length && pageNumber >= 0)
            return true;
        else
            return false;
    }

    function applyPageTurnStrategy(position, checkStartArea) {
        var nextPageTurnStrategy = new NextPageTurnStrategy();
        var previousPageTurnStrategy = new PreviousPageTurnStrategy();

        var result = false;
        var nextPageTurnCheckAreaFun;
        var previousPageTurnCheckAreaFun;

        if (checkStartArea) {
            nextPageTurnCheckAreaFun = nextPageTurnStrategy.isInStartArea.bind(nextPageTurnStrategy);
            previousPageTurnCheckAreaFun = previousPageTurnStrategy.isInStartArea.bind(previousPageTurnStrategy);
        } else {
            nextPageTurnCheckAreaFun = nextPageTurnStrategy.isInFoldArea.bind(nextPageTurnStrategy);
            previousPageTurnCheckAreaFun = previousPageTurnStrategy.isInFoldArea.bind(previousPageTurnStrategy);
        }

        if (checkPageTurnStrategyApplicable(position, nextPageTurnStrategy, nextPageTurnCheckAreaFun)) {
            result = true;
            pageTurnStrategy = nextPageTurnStrategy;
            setPageTurnSizes();
        } else if (checkPageTurnStrategyApplicable(position, previousPageTurnStrategy, previousPageTurnCheckAreaFun)) {
            result = true;
            pageTurnStrategy = previousPageTurnStrategy;
            setPageTurnSizes();
        }

        return result;
    }

    function checkPageTurnStrategyApplicable(position, pageStrategy, checkAreaFun) {
        if (checkAreaFun(position) && checkPageInBoundry(pageStrategy.getTurnPageLeftNumber()))
            return true;

    }

    function onMouseDown(wrapper, id, x, y) {
        dirtyCount = MAX_DIRTY_COUNT;
        if (isTurning)
            return;

        testMouse.x = x;
        testMouse.y = y;
        if (!isMouseOver)
            isMouseOver = applyPageTurnStrategy(testMouse, true);
        isDrag = isMouseOver;
    }

    function onMouseMove(wrapper, id, x, y) {
        dirtyCount = MAX_DIRTY_COUNT;
        if (isTurning)
            return;
        
        testMouse.x = x;
        testMouse.y = y;

        if (isDrag) {
            mouse.x = testMouse.x;
            mouse.y = testMouse.y;
            if (pageTurnStrategy.isInTurnArea(mouse)) {
                startTurn();
            }
        } else if ((isMouseOver = (isMouseOver && pageTurnStrategy.isInFoldArea(testMouse))) || applyPageTurnStrategy(testMouse, false)) {
            // when mouse is over the start area
            // start flipping animation
            isMouseOver = true;
            mouse.x = testMouse.x;
            mouse.y = testMouse.y;
        } else {
            mouse = pageTurnStrategy.createMousePoint();
        }
    }

    function onMouseUp(wrapper, id, x, y) {
        dirtyCount = MAX_DIRTY_COUNT;
        if (isTurning)
            return;

        if (pageTurnStrategy.isInAllowTurnArea(mouse)) {
            startTurn();
            isMouseOver = false;
        } else if (checkPageTurnStrategyApplicable(mouse, pageTurnStrategy, pageTurnStrategy.isInFoldArea.bind(pageTurnStrategy))) {
            isMouseOver = true;
        } else {
            mouse = pageTurnStrategy.createMousePoint();
            isMouseOver = false;
        }

        isMouseDown = false;
        isDrag = false;
    }

    var NextPageTurnStrategy = function() {
    };

    NextPageTurnStrategy.prototype.createEdgeBottomPoint = function() {
        return new Point(pageLeft + pageHalfWidth * 2, pageTop + pageHeight);
    };

    NextPageTurnStrategy.prototype.createCornerPoint = function() {
        return new Point(edgeBottom.x - 1, edgeBottom.y - 1);
    };

    NextPageTurnStrategy.prototype.createMousePoint = function() {
        return new Point(edgeBottom.x - 1, edgeBottom.y - 1);
    };

    NextPageTurnStrategy.prototype.createFollowPoint = function() {
        return new Point(edgeBottom.x - 1, edgeBottom.y - 1);
    };

    NextPageTurnStrategy.prototype.calcPageAngle = function() {
        return Math.atan2(tangentBottom.y - corner.y, tangentBottom.x - corner.x);
    };

    NextPageTurnStrategy.prototype.calcClipAngle = function() {
        return Math.atan2(tangentBottom.y - bisector.y, tangentBottom.x - bisector.x);
    };

    NextPageTurnStrategy.prototype.calcClipPoint0 = function() {
        return new Point(tangentBottom.x, tangentBottom.y + DIAGONAL);
    };

    NextPageTurnStrategy.prototype.calcClipPoint1 = function() {
        return new Point(tangentBottom.x - DIAGONAL, tangentBottom.y + DIAGONAL);
    };

    NextPageTurnStrategy.prototype.calcClipPoint2 = function() {
        return new Point(tangentBottom.x - DIAGONAL, tangentBottom.y - DIAGONAL);
    };

    NextPageTurnStrategy.prototype.calcClipPoint3 = function() {
        return new Point(tangentBottom.x, tangentBottom.y - DIAGONAL);
    };

    NextPageTurnStrategy.prototype.getTurnPageLeftNumber = function() {
        return nextPageLeft;
    };

    NextPageTurnStrategy.prototype.getTurnPageRightNumber = function() {
        return nextPageRight;
    };

    NextPageTurnStrategy.prototype.getTurnPageLeft = function() {
        if (checkPageInBoundry(nextPageLeft))
            return pages[nextPageLeft];
        return imgBlank;
    };

    NextPageTurnStrategy.prototype.getTurnPageRight = function() {
        if (checkPageInBoundry(nextPageRight))
            return pages[nextPageRight];
        return imgBlank;
    };

    NextPageTurnStrategy.prototype.getTurnedPagePosition = function() {
        return new Point(0, -pageHeight);
    };

    NextPageTurnStrategy.prototype.getEdgeOffsetPoint = function() {
        return new Point(WIDTH, 0);
    };

    NextPageTurnStrategy.prototype.getMouseDestinationPoint = function() {
        return new Point(edgeBottom.x - pageHalfWidth * 2, edgeBottom.y);
    };

    NextPageTurnStrategy.prototype.isInStartArea = function(position) {
        var edgeBottomLoc = this.createEdgeBottomPoint();
        if ((position.x > (edgeBottomLoc.x - pageHalfWidth)) && testMouse.x < edgeBottomLoc.x) {
            if ((position.y > (edgeBottomLoc.y - pageHalfHeight)) && position.y < edgeBottomLoc.y)
                return true;
        }
        return false;
    };

    NextPageTurnStrategy.prototype.isInFoldArea = function(position) {
        var edgeBottomLoc = this.createEdgeBottomPoint();
        if (position.x > (edgeBottomLoc.x - foldBorder) && position.x < edgeBottomLoc.x) {
            if (position.y > (edgeBottomLoc.y - foldBorder) && position.y < edgeBottomLoc.y) {
                return true;
            }
        }
        return false;
    };

    NextPageTurnStrategy.prototype.isInTurnArea = function(position) {

        if (position.x < (pageLeft + pageHalfWidth * .25))
            return true;
        return false;
    };

    NextPageTurnStrategy.prototype.isInAllowTurnArea = function(position) {
        if (position.x < edgeBottom.x - 150)
            return true;
        return false;
    };

    NextPageTurnStrategy.prototype.calcDistanceFromTarget = function(position) {
        return Math.sqrt((position.x - pageLeft) * (position.x - pageLeft) + ((pageHeight + pageTop) - position.y) * ((pageHeight + pageTop) - position.y)) / pageHalfWidth;
    };

    NextPageTurnStrategy.prototype.getImgShadowCurvePosition = function(position) {
        // FIXME: Replace -WIDTH*.1 by some calculations
        return new Point(-imgShadowCurve.width, WIDTH * .1 - DIAGONAL);
    };

    NextPageTurnStrategy.prototype.calcImgShadowCurveAngle = function(angle) {
        return angle;
    };

    NextPageTurnStrategy.prototype.getImgShadowBackPosition = function(position) {
        // FIXME: Replace -WIDTH*.1 by some calculations
        return new Point(0, WIDTH * .1 - DIAGONAL);
    };

    NextPageTurnStrategy.prototype.calcImgShadowBackAngle = function(angle) {
        return angle;
    };

    var PreviousPageTurnStrategy = function() {
    };

    PreviousPageTurnStrategy.prototype.createEdgeBottomPoint = function() {
        return new Point(pageLeft, pageTop + pageHeight);
    };

    PreviousPageTurnStrategy.prototype.createCornerPoint = function() {
        return new Point(edgeBottom.x + 1, edgeBottom.y - 1);
    };

    PreviousPageTurnStrategy.prototype.createMousePoint = function() {
        return new Point(edgeBottom.x + 1, edgeBottom.y - 1);
    };

    PreviousPageTurnStrategy.prototype.createFollowPoint = function() {
        return new Point(edgeBottom.x + 1, edgeBottom.y - 1);
    };

    PreviousPageTurnStrategy.prototype.calcPageAngle = function() {
        return Math.PI + Math.atan2(tangentBottom.y - corner.y, tangentBottom.x - corner.x);
    };

    PreviousPageTurnStrategy.prototype.calcClipAngle = function() {
        return Math.atan2(tangentBottom.y - bisector.y, tangentBottom.x - bisector.x);
    };

    PreviousPageTurnStrategy.prototype.calcClipPoint0 = function() {
        return new Point(tangentBottom.x, tangentBottom.y + DIAGONAL);
    };

    PreviousPageTurnStrategy.prototype.calcClipPoint1 = function() {
        return new Point(tangentBottom.x + DIAGONAL, tangentBottom.y + DIAGONAL);
    };

    PreviousPageTurnStrategy.prototype.calcClipPoint2 = function() {
        return new Point(tangentBottom.x + DIAGONAL, tangentBottom.y - DIAGONAL);
    };

    PreviousPageTurnStrategy.prototype.calcClipPoint3 = function() {
        return new Point(tangentBottom.x, tangentBottom.y - DIAGONAL);
    };

    PreviousPageTurnStrategy.prototype.getTurnPageLeftNumber = function() {
        return previousPageLeft;
    };

    PreviousPageTurnStrategy.prototype.getTurnPageRightNumber = function() {
        return previousPageRight;
    };

    PreviousPageTurnStrategy.prototype.getTurnPageLeft = function() {
        if (checkPageInBoundry(previousPageLeft))
            return pages[previousPageLeft];
        return imgBlank;
    };

    PreviousPageTurnStrategy.prototype.getTurnPageRight = function() {
        if (checkPageInBoundry(previousPageRight))
            return pages[previousPageRight];
        return imgBlank;
    };

    PreviousPageTurnStrategy.prototype.getTurnedPagePosition = function() {
        return new Point(-2 * pageHalfWidth, -pageHeight);
    };

    PreviousPageTurnStrategy.prototype.getEdgeOffsetPoint = function() {
        return new Point(0, 0);
    };

    PreviousPageTurnStrategy.prototype.getMouseDestinationPoint = function() {
        return new Point(edgeBottom.x + pageHalfWidth * 2, edgeBottom.y);
    };

    PreviousPageTurnStrategy.prototype.isInStartArea = function(position) {
        var edgeBottomLoc = this.createEdgeBottomPoint();
        if ((position.x < (edgeBottomLoc.x + pageHalfWidth)) && position.x > edgeBottomLoc.x) {
            if ((position.y > (edgeBottomLoc.y - pageHalfHeight)) && position.y < edgeBottomLoc.y)
                return true;
        }
    };

    PreviousPageTurnStrategy.prototype.isInFoldArea = function(position) {
        var edgeBottomLoc = this.createEdgeBottomPoint();
        if (position.x < (edgeBottomLoc.x + foldBorder) && position.x > edgeBottomLoc.x) {
            if (position.y > (edgeBottomLoc.y - foldBorder) && position.y < edgeBottomLoc.y)
                return true;
        }
    };

    PreviousPageTurnStrategy.prototype.isInTurnArea = function(position) {

        if (position.x > (pageLeft + 2 * pageHalfWidth) * 0.75)
            return true;
        return false;
    };

    PreviousPageTurnStrategy.prototype.isInAllowTurnArea = function(position) {
        if (position.x > edgeBottom.x + 150)
            return true;
        return false;
    };

    PreviousPageTurnStrategy.prototype.calcDistanceFromTarget = function(position) {
        var localPageRight = pageWidth + pageLeft;
        return Math.sqrt((position.x - localPageRight) * (position.x - localPageRight) + ((pageHeight + pageTop) - position.y) * ((pageHeight + pageTop) - position.y)) / pageHalfWidth;
    };

    PreviousPageTurnStrategy.prototype.getImgShadowCurvePosition = function(position) {
        // FIXME: Replace -WIDTH*.1 by some calculations
        return new Point(-imgShadowCurve.width, - WIDTH * .1);
    };

    PreviousPageTurnStrategy.prototype.calcImgShadowCurveAngle = function(angle) {
        return angle + Math.PI;
    };

    PreviousPageTurnStrategy.prototype.getImgShadowBackPosition = function(position) {
        // FIXME: Replace -WIDTH*.1 by some calculations
        return new Point(0, - WIDTH * .1);
    };

    PreviousPageTurnStrategy.prototype.calcImgShadowBackAngle = function(angle) {
        return angle + Math.PI;
    };

    // *****************************************************************************************************************
    // *
    // THESE FUNCTIONS ARE PULLED FROM A GREAT SET OF ARTICLES ON HANDLING TOUCH
    // IN IE AND IPAD BY TED JOHNSON AT IEBLOGS.
    // ORIGINAL ARTICLE AND EXPLANATION CAN BE FOUND HERE:
    // http://blogs.msdn.com/b/ie/archive/2011/10/19/handling-multi-touch-and-mouse-input-in-all-browsers.aspx
    // *
    // *****************************************************************************************************************

    function PointerFusion(target, pointerStart, pointerMove, pointerEnd) {

        var me = this;
        var lastXYById = {};

        // Opera doesnt have Object.keys so we use this wrapper
        function NumberOfKeys(theObject) {
            if (Object.keys)
                return Object.keys(theObject).length;
            var n = 0;
            for ( var key in theObject)
                ++n;
            return n;
        }

        // IE10s implementation in the Windows Developer Preview requires doing
        // all of this
        // Not all of these methods remain in the Windows Consumer Preview,
        // hence the tests for method existence.
        function PreventDefaultManipulationAndMouseEvent(evtObj) {
            if (evtObj.preventDefault)
                evtObj.preventDefault();

            if (evtObj.preventManipulation)
                evtObj.preventManipulation();

            if (evtObj.preventMouseEvent)
                evtObj.preventMouseEvent();
        }

        // we send target-relative coordinates to the draw functions
        // this calculates the delta needed to convert pageX/Y to offsetX/Y
        // because offsetX/Y dont exist in the TouchEvent object or in Firefoxs
        // MouseEvent object
        function ComputeDocumentToElementDelta(theElement) {
            var elementLeft = 0;
            var elementTop = 0;

            for ( var offsetElement = theElement; offsetElement != null; offsetElement = offsetElement.offsetParent) {
                // the following is a major hack for versions of IE less than 8
                // to avoid an apparent problem on the IEBlog with
                // double-counting the offsets
                // this may not be a general solution to IE7s problem with
                // offsetLeft/offsetParent
                if (navigator.userAgent.match(/\bMSIE\b/) && (!document.documentMode || document.documentMode < 8) && offsetElement.currentStyle.position == "relative" && offsetElement.offsetParent
                        && offsetElement.offsetParent.currentStyle.position == "relative" && offsetElement.offsetLeft == offsetElement.offsetParent.offsetLeft) {
                    // add only the top
                    elementTop += offsetElement.offsetTop;
                } else {
                    elementLeft += offsetElement.offsetLeft;
                    elementTop += offsetElement.offsetTop;
                }
            }
            return {
                x : elementLeft,
                y : elementTop
            };
        }

        // function needed because IE versions before 9 did not define pageX/Y
        // in the MouseEvent object
        function EnsurePageXY(eventObj) {
            if (typeof eventObj.pageX == 'undefined') {
                // initialize assuming our source element is our target
                eventObj.pageX = eventObj.offsetX + documentToTargetDelta.x;
                eventObj.pageY = eventObj.offsetY + documentToTargetDelta.y;

                if (eventObj.srcElement.offsetParent == target && document.documentMode && document.documentMode == 8 && eventObj.type == "mousedown") {
                    // source element is a child piece of VML, were in IE8, and
                    // weve not called setCapture yet - add the origin of the
                    // source element
                    eventObj.pageX += eventObj.srcElement.offsetLeft;
                    eventObj.pageY += eventObj.srcElement.offsetTop;
                } else if (eventObj.srcElement != target && !document.documentMode || document.documentMode < 8) {
                    // source element isnt the target (most likely its a child
                    // piece of VML) and were in a version of IE before IE8 -
                    // the offsetX/Y values are unpredictable so use the
                    // clientX/Y values and adjust by the scroll offsets of its
                    // parents
                    // to get the document-relative coordinates (the same as
                    // pageX/Y)
                    var sx = -2, sy = -2; // adjust for old IEs 2-pixel border
                    for ( var scrollElement = eventObj.srcElement; scrollElement != null; scrollElement = scrollElement.parentNode) {
                        sx += scrollElement.scrollLeft ? scrollElement.scrollLeft : 0;
                        sy += scrollElement.scrollTop ? scrollElement.scrollTop : 0;
                    }

                    eventObj.pageX = eventObj.clientX + sx;
                    eventObj.pageY = eventObj.clientY + sy;
                }
            }
        }

        // cache the delta from the document to our event target (reinitialized
        // each mousedown/MSPointerDown/touchstart)
        var documentToTargetDelta;
        
        /**
         * Refreshes PointerFusion state in case environment has changed
         */
        me.refreshState = function() {
            documentToTargetDelta = ComputeDocumentToElementDelta(target);
        }

        me.refreshState();

        // functions to convert document-relative coordinates to target-relative
        // and constrain them to be within the target
        function targetRelativeX(px) {
            return Math.max(0, Math.min(px - documentToTargetDelta.x, target.offsetWidth));
        }
        ;
        function targetRelativeY(py) {
            return Math.max(0, Math.min(py - documentToTargetDelta.y, target.offsetHeight));
        }
        ;

        // common event handler for the mouse/pointer/touch models and their
        // down/start, move, up/end, and cancel events
        function DoEvent(theEvtObj) {

            // optimize rejecting mouse moves when mouse is up
            // if (theEvtObj.type == "mousemove" && NumberOfKeys(lastXYById) ==
            // 0)
            // return;

            PreventDefaultManipulationAndMouseEvent(theEvtObj);

            var pointerList = theEvtObj.changedTouches ? theEvtObj.changedTouches : [ theEvtObj ];
            for ( var i = 0; i < pointerList.length; ++i) {
                var pointerObj = pointerList[i];
                var pointerId = (typeof pointerObj.identifier != 'undefined') ? pointerObj.identifier : (typeof pointerObj.pointerId != 'undefined') ? pointerObj.pointerId : 1;

                // use the pageX/Y coordinates to compute target-relative
                // coordinates when we have them (in ie < 9, we need to do a
                // little work to put them there)
                EnsurePageXY(pointerObj);
                var pageX = pointerObj.pageX;
                var pageY = pointerObj.pageY;

                if (theEvtObj.type.match(/(start|down)$/i)) {
                    // clause for processing MSPointerDown, touchstart, and
                    // mousedown

                    // refresh the document-to-target delta on start in case the
                    // target has moved relative to document
                    documentToTargetDelta = ComputeDocumentToElementDelta(target);

                    // protect against failing to get an up or end on this
                    // pointerId
                    if (lastXYById[pointerId]) {
                        if (pointerEnd)
                            pointerEnd(target, pointerId);
                        delete lastXYById[pointerId];
                    }

                    if (pointerStart)
                        pointerStart(target, pointerId, targetRelativeX(pageX), targetRelativeY(pageY));

                    // init last page positions for this pointer
                    lastXYById[pointerId] = {
                        x : pageX,
                        y : pageY
                    };

                    // in the Microsoft pointer model, set the capture for this
                    // pointer
                    // in the mouse model, set the capture or add a
                    // document-level event handlers if this is our first down
                    // point
                    // nothing is required for the iOS touch model because
                    // capture is implied on touchstart
                    if (target.msSetPointerCapture)
                        target.msSetPointerCapture(pointerId);
                    else if (theEvtObj.type == "mousedown" && NumberOfKeys(lastXYById) == 1) {
                        if (useSetReleaseCapture)
                            target.setCapture(true);
                    }
                } else if (theEvtObj.type.match(/move$/i)) {
                    // clause handles mousemove, MSPointerMove, and touchmove

                    if (!lastXYById[pointerId] || !(lastXYById[pointerId].x == pageX && lastXYById[pointerId].y == pageY)) {
                        // only extend if the pointer is down and its not the
                        // same as the last point

                        if (pointerMove)
                            pointerMove(target, pointerId, targetRelativeX(pageX), targetRelativeY(pageY));

                        // update last page positions for this pointer
                        lastXYById[pointerId] = {
                            x : pageX,
                            y : pageY
                        };
                    }
                } else if (lastXYById[pointerId] && theEvtObj.type.match(/(up|end|cancel)$/i)) {
                    // clause handles up/end/cancel

                    if (pointerEnd)
                        pointerEnd(target, pointerId);

                    // delete last page positions for this pointer
                    delete lastXYById[pointerId];

                    // in the Microsoft pointer model, release the capture for
                    // this pointer
                    // in the mouse model, release the capture or remove
                    // document-level event handlers if there are no down points
                    // nothing is required for the iOS touch model because
                    // capture is implied on touchstart
                    if (target.msReleasePointerCapture)
                        target.msReleasePointerCapture(pointerId);
                    else if (theEvtObj.type == "mouseup" && NumberOfKeys(lastXYById) == 0) {
                        if (useSetReleaseCapture)
                            target.releaseCapture();
                    }
                }
            }
        }

        var useSetReleaseCapture = false;

        if (window.navigator.msPointerEnabled) {
            // Microsoft pointer model
            target.addEventListener("MSPointerDown", DoEvent, false);
            target.addEventListener("MSPointerMove", DoEvent, false);
            target.addEventListener("MSPointerUp", DoEvent, false);
            target.addEventListener("MSPointerCancel", DoEvent, false);

            // css way to prevent panning in our target area
            if (typeof target.style.msContentZooming != 'undefined')
                target.style.msContentZooming = "none";

            // new in Windows Consumer Preview: css way to prevent all built-in
            // touch actions on our target
            // without this, you cannot touch draw on the element because IE
            // will intercept the touch events
            if (typeof target.style.msTouchAction != 'undefined')
                target.style.msTouchAction = "none";

        } else if (target.addEventListener) {
            // iOS touch model
            target.addEventListener("touchstart", DoEvent, false);
            target.addEventListener("touchmove", DoEvent, false);
            target.addEventListener("touchend", DoEvent, false);
            target.addEventListener("touchcancel", DoEvent, false);

            // mouse model
            target.addEventListener("mousedown", DoEvent, false);

            // mouse model with capture
            // rejecting gecko because, unlike ie, firefox does not send events
            // to target when the mouse is outside target
            if (target.setCapture && !window.navigator.userAgent.match(/\bGecko\b/)) {
                useSetReleaseCapture = true;

            }

            target.addEventListener("mousemove", DoEvent, false);
            target.addEventListener("mouseup", DoEvent, false);

        } else if (target.attachEvent && target.setCapture) {
            // legacy IE mode - mouse with capture
            useSetReleaseCapture = true;
            target.attachEvent("onmousedown", function() {
                DoEvent(window.event);
                window.event.returnValue = false;
                return false;
            });
            target.attachEvent("onmousemove", function() {
                DoEvent(window.event);
                window.event.returnValue = false;
                return false;
            });
            target.attachEvent("onmouseup", function() {
                DoEvent(window.event);
                window.event.returnValue = false;
                return false;
            });

        }

    }

    // From Prototype.js
    if (!Function.prototype.bind) { // check if native implementation available
        Function.prototype.bind = function() {
            var fn = this, args = Array.prototype.slice.call(arguments), object = args.shift();
            return function() {
                return fn.apply(object, args.concat(Array.prototype.slice.call(arguments)));
            };
        };
    }

    init();

}