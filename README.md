PageFlipper uses JavaScript and HTML5 to simulate flipping pages. It is based on Rick Barraza tutorial (see http://rbarraza.com/html5-canvas-pageflip)

Available under the Apache License (v2).

### Basic usage: ###

Below code creates PageFlipper with three pages and page size 500x375.

```
#!html
...
<script type="text/javascript" src="pageflipper.js"></script>

<body onload="PageFlipper(500, 375, ['img1.jpg', 'img2.jpg', 'img3.jpg']);">

<div class="DivPageFlip"></div>
...
```

NOTE: <div> size:

* width = 2*pageWidth + padding
* height = pageHeight + padding

See working example:

http://balbusm.bitbucket.io/pageflipper/example.html

### Advanced usage: ###

PageFlipper can be run with the following parameters:

```
#!javascript
PageFlipper(
    pageWidth, // single image width
    pageHeight, // single image height
    ['img1.jpg', 'img2.jpg', 'img3.jpg'], // array of images to flip
    { // configuration object, optional
        debug, // enables debugging, default false
        shadowBlackImg, // path to back page shadow image, default shadowBack.png
        shadowCurveImg, // path to front page shadow image, default shadowCurve.png
        loadingImg, // path to spinner image, default loading.gif 
        divPageFlipClass // div class name that PageFlipper is looking for, default DivPageFlip
    }
);
```

